FROM node:lts
COPY . /app
RUN npm ci
RUN npm i -g @angular/cli

CMD ng serve